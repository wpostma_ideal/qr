program SimpleQRVCL;

uses
  Vcl.Forms,
  SimpleMain in 'SimpleMain.pas' {frmSimpleMain};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmSimpleMain, frmSimpleMain);
  Application.Run;
end.
