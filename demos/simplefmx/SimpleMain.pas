unit SimpleMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.Edit, FMX.Objects;

type
  TfrmSimpleMain = class(TForm)
    txtData: TEdit;
    imgQR: TImage;
    procedure txtDataChangeTracking(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSimpleMain: TfrmSimpleMain;

implementation

uses qr.code.fmx;

{$R *.fmx}

procedure TfrmSimpleMain.txtDataChangeTracking(Sender: TObject);
var
  bmp : TQRBitmap;
begin
  bmp := TQRBitmap.Create(txtData.Text, imgQR.Width, imgQR.Height);
  try
    bmp.ShowBorder := True;
    imgQR.Bitmap := bmp;
  finally
    bmp.Free;
  end;
end;

end.
