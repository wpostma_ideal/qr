object frmQRMain: TfrmQRMain
  Left = 0
  Top = 0
  Caption = 'frmQRMain'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    784
    494)
  PixelsPerInch = 96
  TextHeight = 13
  object imgQR: TImage
    Left = 24
    Top = 48
    Width = 420
    Height = 420
    Anchors = [akLeft, akTop, akRight]
  end
  object txtData: TEdit
    Left = 271
    Top = 8
    Width = 377
    Height = 21
    TabOrder = 1
    Text = 'HELLO WORLD'
    OnChange = txtDataChange
  end
  object cbECL: TComboBox
    Left = 24
    Top = 8
    Width = 65
    Height = 21
    Style = csDropDownList
    TabOrder = 0
    OnChange = txtDataChange
    Items.Strings = (
      'Auto ECL'
      'Low'
      'Medium'
      'Quartile'
      'High')
  end
  object txtWidth: TEdit
    Left = 701
    Top = 8
    Width = 57
    Height = 21
    TabOrder = 2
    Text = '500'
    OnChange = txtDataChange
  end
  object chkGrid: TCheckBox
    Left = 654
    Top = 8
    Width = 41
    Height = 17
    Caption = 'Grid'
    TabOrder = 3
    OnClick = txtDataChange
  end
  object txtDetails: TMemo
    Left = 450
    Top = 48
    Width = 308
    Height = 420
    Anchors = [akTop, akRight, akBottom]
    Lines.Strings = (
      'Memo1')
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 4
    WordWrap = False
  end
  object cbMask: TComboBox
    Left = 95
    Top = 8
    Width = 82
    Height = 21
    Style = csDropDownList
    ItemIndex = 8
    TabOrder = 5
    Text = 'Auto Mask'
    OnChange = txtDataChange
    Items.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      'Auto Mask')
  end
  object cbVersion: TComboBox
    Left = 183
    Top = 8
    Width = 82
    Height = 21
    Style = csDropDownList
    ItemIndex = 0
    TabOrder = 6
    Text = 'Auto Version'
    OnChange = txtDataChange
    Items.Strings = (
      'Auto Version'
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9'
      '10'
      '11'
      '12'
      '13'
      '14'
      '15'
      '16'
      '17'
      '18'
      '19'
      '20'
      '21'
      '22'
      '23'
      '24'
      '25'
      '26'
      '27'
      '28'
      '29'
      '30'
      '31'
      '32'
      '33'
      '34'
      '35'
      '36'
      '37'
      '38'
      '39'
      '40')
  end
end
