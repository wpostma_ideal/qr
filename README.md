# qr code generator for Delphi

To generate a QR code, select the framework unit you need:

* qr.code.vcl - for VCL projects
* qr.code.fmx - for Firemonkey projects

Each unit contains a TQRBitmap object which can be used as follows to generate a 600px bitmap of qr encoded user supplied text:

```pascal
var
  bmp : TQRBitmap;
begin
  bmp := TQRBitmap.Create(Edit1.Text, 600, 600);
  try
    Image1.Picture.Graphic := bmp;
  finally
    bmp.Free;
  end;
end;
```

If you want more control over the image or would like to do custom drawing, the main qr generation object can be used directly.

```pascal
var
  qr : TQRCode;
begin
  qr:= TQRCode.Create;
  try
    qr.OnPaint :=
      procedure(Width, Height : integer; BlackRects : TArray<TRect>)
	  begin
	    // paint here using the supplied width and height as well as the array of rectangles that should be painted black.
  	  end;
    qr.BeginUpdate; // if you fail to use BeginUpdate, the code will rerender with each of the changes below.
    try
      qr.Mode := TMode.Auto;
      qr.Version := TVersion.Auto;
      qr.ECL := TErrorCorrectionLevel.Auto;
      qr.Text := 'Some Data To Encode';
    finally
      qr.EndUpdate;  // will cause the OnPaint handler to get called
    end;
  finally
    qr.Free;
  end;
 end;
```

## Demos

Four Demos are provided for your assistance...

* SimpleQRVCL - A VCL Demo showing a qr encoder using all defaults.
* SimpleQRFMX - An FMX Demo showing a qr encoder using all defaults.
* QREncoderVCL - A VCL Demo showing an encoder with a high level of customization.
* QREncoderFMX - An FMX Demo Showing an encoder with a high leverl of customization.

## Known Limitations

The only supported encodings right now are Numeric, AlphaNumeric and Byte. Kanji and other advanced encodings are not yet supported.  Full unicode support is supported by most readers via reading Byte encoding as UTF8.  When you assign a string that does not qualify as Numeric or AlphaNumeric, UTF8 is used and stored as byte encoding.

## License

> Copyright (c) 2019 by Jason Southwell
>
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
> 
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.